\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{phdthesis}[2021/01/01 v0.1 Phd thesis class]


\newcommand{\headlinecolor}{\normalcolor}
\LoadClass[twocolumn]{article}
\RequirePackage{xcolor}
\definecolor{slcolor}{HTML}{882B21}
