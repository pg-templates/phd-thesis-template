DIR=.
EXAMPLE="./examples/example.tex"

LATEXMK=latexmk
LATEXMK_OPTIONS=-bibtex -pdf -pdflatex="pdflatex -interaction=nonstopmode" -use-make

.PHONY: all example clean

all: example

example:
	$(LATEXMK) $(LATEXMK_OPTIONS) $(DIR)/examples/example.tex

#FIXME: clean should clean all generated pdf not be fixed for example.tex
clean:
	$(LATEXMK) -CA $(DIR)/examples/example.tex
